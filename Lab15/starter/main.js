'use strict';

document.addEventListener("DOMContentLoaded", function(){
  let tbar =document.querySelector('.thumb-bar');
  const images = [];
  for(let i =0;i<5;i++){
    images.push(`images/pic${i + 1}.jpg`);
  }

  const imgElement = images.map((n) => {
    let img = document.createElement('img');
    img.src = n;
    return img;
  });

  imgElement.forEach((image) => {
    tbar.appendChild(image);
  });
  tbar.addEventListener("click",function(e){
    document.querySelector('.displayed-img').src = e.target.src;
  });
});