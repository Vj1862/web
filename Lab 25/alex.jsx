"use strict";
const { useState } = React;
function ShoppingList({list}) {
  return (
    <ul style={{border : "green double 8px"}}>
      <ShoppingListItem list={list}/>
  </ul>);
}

function ShoppingListItem({list}){
  return(
    list.map((listItem,idx) => <li key={idx}>{idx}: {listItem}</li>)
  );
}

function App(){
  const [list,setList] = useState(["Beans","Jam","Bread","juice"]);
  const [currVal,setCurrVal] = useState("");
  console.log(currVal);
  console.log(list);
  return(
    <div>
      <form onSubmit={(e) => {
        e.preventDefault();
      }}>
        <label>Sam's Shopping List</label>
        <input id="input" type="text" onChange={(e) => {setCurrVal(e.target.value);}} />
        <button id="btn" type="submit" onClick={(e) => {
          e.preventDefault();
          setList([...list,currVal]);
        }}>Submit</button>
      </form>
      <ShoppingList list={list}/>
    </div>
  );
}
ReactDOM.render(<App/>,document.querySelector('#root'));

