"use strict";
document.addEventListener("DOMContentLoaded",function(){
  const button = document.querySelector("button");
  const uname = document.getElementById("uname");

  button.addEventListener("click",function(){
    if(!/^[A-Z].{5,20}/.test(uname.value)){
      uname.setCustomValidity("Must start with Upper case Letter");
    } else{
      uname.setCustomValidity("");
    }
  });
});
// Write it all in your DOMContentLoaded event.