document.addEventListener("DOMContentLoaded", function() {
  "use strict";

  //const main = document.getElementsByTagName("main")[0];

  const listCountries = document.querySelector(".countries");
  
  const head = document.querySelector("h1");
  const headText = head.textContent;

  function loadCountries(){
    for (let i in COUNTRIES){

      //Create element
      const para = document.createElement('p');
      para.setAttribute('id',COUNTRIES[i]);
      const text = document.createTextNode(COUNTRIES[i]);
      para.appendChild(text);
      listCountries.appendChild(para);

      //Asign colors
      const country = document.getElementById(COUNTRIES[i]);

      if(parseInt(COUNTRIES[i].length) % 3 === 0){
        country.style.background = '#98FB98';
      }
      if(parseInt(COUNTRIES[i].length) % 7 === 0){
        country.style.background = '#DAA520';
      }
      console.log(country.textContent);

      //Addeventlistners

      country.addEventListener("click",function(e){

        head.textContent = e.target.textContent;

      });
    // country.addEventListener("mouseleave",function(){

    //   head.textContent = headText;

    // });
    }
  }
  loadCountries();
});