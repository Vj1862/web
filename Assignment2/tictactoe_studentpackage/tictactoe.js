"use strict";
document.addEventListener("DOMContentLoaded",function(){
  let Player = "X";

  const table = document.getElementById("gameboard");
  const curPlayer = document.getElementById("curplayer");

  const tr = document.getElementsByTagName("tr");
  const td = document.getElementsByTagName("td");

  const Wplayer = document.getElementById("winningplayer");

  const button = document.getElementById("reset");

  let values;
  let winner;
  button.addEventListener("click",function(values){
    values = [];

    for (let i = 0;i<tr.length;i++){
      for (let j = 0;j<tr[i].children.length;j++){
        tr[i].children[j].removeAttribute("class");
        tr[i].children[j].textContent ="";
        winner = "";
        document.querySelector("aside").style.visibility = "hidden";
        Wplayer.textContent ="";
      }
    }
    Player = "X";
    curPlayer.textContent = Player;
  });

  table.addEventListener("click",function(e){
    
    if(document.querySelector("aside").style.visibility === "visible"){
      return;
    }
    if(e.target.textContent === ""){
      e.target.textContent = Player;
      e.target.setAttribute("class",Player.toLowerCase() + "square");
    }
    else{
      return;
    }
    //Checking winner
    values = [];
    for (let i = 0;i<tr.length;i++){
      for (let j = 0;j<tr[i].children.length;j++){
        if(tr[i].children[j].textContent === ""){
          values.push(false);
        }
        else{
          values.push(tr[i].children[j].textContent);
        }
      }
    }
    winner = checkBoard(...values);
    if(winner !== false){
      showWinner(winner);
      return;
    }
    else if (values.filter((v) => typeof(v) !== "string").length == 0){
      document.querySelector("aside").getElementsByTagName('h2')[0].textContent = "It's a tie!";
      document.querySelector("aside").style.visibility = "visible";
      document.getElementsByTagName("h2")[0].style.visibility = "hidden";
    }
    if(Player === "X"){
      Player = "O";
      curPlayer.textContent = Player;
    }
    else if(Player === "O"){
      Player = "X";
      curPlayer.textContent = Player;
    }
  });

  function showWinner(winner){
    Wplayer.textContent = winner;
    document.querySelector("aside").style.visibility = "visible";
    document.getElementsByTagName("h2")[0].style.visibility = "hidden";
  }
});

const tbody = document.querySelector("tbody");
// Put your DOMContentLoaded event listener here first.
