"use strict";
document.addEventListener("DOMContentLoaded", function(){
  document.querySelector("#formie").addEventListener("submit",function(e){
    e.preventDefault();
    console.log("michelle");
    const input = document.querySelector("#numba").value;
    fetch(`https://reqres.in/api/unknown/${input}`
    ).then((r)=> r.json()).then((data)=>{
      let li = document.createElement("li");
      li.textContent = data.data.name;
      li.style.color = data.data.color;
      document.querySelector("ul").appendChild(li);
    }).catch((e)=>{
      let li = document.createElement("li");
      li.textContent = e;
      li.style.color = 'red';
      document.querySelector("ul").appendChild(li);
      console.log("blame michelle");
    });
  });
});