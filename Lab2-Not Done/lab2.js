// 1. GET ELEMENT REFERENCES
document.addEventListener("DOMContentLoaded", function() {
  const colorPicker = document.getElementById("color");
  const opacityPicker =document.getElementById("opacity");
  const target = document.getElementById("target");

  // 2. EVENT LISTENERS

    // This event listener will be run whenever the colour picker's value changes.
  colorPicker.addEventListener("input", function(e) {
      document.body.style.backgroundColor = e.target.value;
  });

    // This event listener will be run whenever the opacity picker's value changes.
  opacityPicker.addEventListener("input", function(event) {
    // TODO: replace this note with a line of code that sets the target's opacity to the value found at event.target.value
  });
});

// 3. INITIAL PAGE SETUP

// TODO: replace this note with a line of code that sets the target's background colour to the colour that is.
// TODO: replace this note with a line of code that sets the target's opacity to the opacity
