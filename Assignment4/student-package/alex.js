"use strict";
document.addEventListener("DOMContentLoaded",(e) =>{
  const date = document.querySelector("#date");
  const value = document.querySelector("#type");
  let menu = document.querySelector("#image-menu");
  change(value,date);
  const footDate = document.querySelector("#earth-image-date");
  const footTitle = document.querySelector("#earth-image-title");
  let everything = [];
  document.querySelector("#request_form").addEventListener("submit",function(e){   //select the button
    e.preventDefault();
    menu.textContent = undefined;
    everything = [];
    fetch(`https://epic.gsfc.nasa.gov/api/${value.value}/date/${date.value}`
    ).then((r)=>r.json()).then((data)=>{
      if(data.length === 0){
        menu.textContent = "no data found";
        return;
      }
        for(let alex in data){
          let li = document.querySelector("#image-menu-item").content.cloneNode(true);
          li.children[0].firstChild.textContent = data[alex].date;
          li.children[0].firstChild.setAttribute("alex",alex);
          menu.appendChild(li);
        }
        everything.push(data);
    }).catch((r)=>{
      console.log(`my error is ${r}`);
    });
  });
  menu.addEventListener("click",function(e){
    e.preventDefault();
    if(everything.length === 0 || e.target.tagName !== "SPAN"){
      return;
    }
    console.log(everything);
    let indexes = e.target.getAttribute("alex");
    let img = everything[0][indexes].image;
    let dateArray = date.value.split("-");
    let year = dateArray[0];
    let month = dateArray[1];
    let day = dateArray[2];
    document.querySelector("#earth-image").src = `https://epic.gsfc.nasa.gov/archive/${value.value}/${year}/${month}/${day}/jpg/${img}.jpg`;
    footDate.textContent = everything[0][indexes].date;
    footTitle.textContent = everything[0][indexes].caption;
  });
  value.addEventListener("change",function(e){
    change(value, date);
  });
  function change(value, date) {
    menu.textContent = undefined;
    fetch(`https://epic.gsfc.nasa.gov/api/${value.value}/all`
    ).then((r) => r.json()).then((data) => {
      data.sort((a, b) => { return b.date - a.date; });
      date.setAttribute("max", data[0].date);
      date.value = data[0].date;
    }).catch((r) => {
      console.log(`My error is ${r}`);
    });
  }
});

