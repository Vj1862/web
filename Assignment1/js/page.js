document.addEventListener("DOMContentLoaded", function() {
    const java = "../images/Java-Logo.jpg";
    const JS = "../images/js.png";
    const oracle ="../images/oracle.jpg";

    const JsDesc = "JavaScript, often abbreviated as JS, is a programming language that is one of the core technologies of the World Wide Web, alongside HTML and CSS. <br> <br>As of 2023, 98.7% of websites use JavaScript on the client side for webpage behavior, often incorporating third-party libraries";
    const javaDesc = "Java is a high-level, class-based, object-oriented programming language that is designed to have as few implementation dependencies as possible.";
    const oracleDesc = "Oracle Database is a proprietary multi-model database management system produced and marketed by Oracle Corporation. <br><br>It is a database commonly used for running online transaction processing, data warehousing and mixed database workloads.";

    const JsTeacher = "sam & nasr";
    const javaTeacher = "dan & swetha";
    const oracleTeacher = "giancarlo & mahsa";

    const jsButton = document.getElementById("js");
    const javaButton = document.getElementById("java");
    const oracleButton = document.getElementById("Oracle");

    const teacher = document.getElementById("prof");

    const description = document.getElementById("details");

    const pic = document.getElementById("pic");

    javaButton.addEventListener("click",function(){
        pic.src = java;
        description.textContent = javaDesc;
        teacher.textContent = javaTeacher;
    });

    jsButton.addEventListener("click",function(){
        pic.src = JS;
        description.textContent = JsDesc;
        teacher.textContent = JsTeacher;
    });

    oracleButton.addEventListener("click",function(){
        pic.src = oracle;
        description.textContent = oracleDesc;
        teacher.textContent = oracleTeacher;
    });

});