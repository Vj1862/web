"use strict";
document.addEventListener("DOMContentLoaded",(e) =>{
  let menu = document.querySelector("#image-menu");
  let arr1 = ['natural','enhanced','aerosol','cloud'];
  for(let alex of arr1){
    const option = document.createElement("option");
    option.value = alex;
    option.textContent = alex;
    document.querySelector('#type').appendChild(option);
  }
  const date = document.querySelector("#date");
  const value = document.querySelector("#type");
  const footDate = document.querySelector("#earth-image-date");
  const footTitle = document.querySelector("#earth-image-title");
  const EpicApplication = {
    imageType: null,
    imageCache: {}
  };
  window.Epic = EpicApplication;
  const maxDate = {};
  change(value,date);
  document.querySelector("#request_form").addEventListener("submit",function(e){
    e.preventDefault();
    menu.textContent = undefined;
    if(EpicApplication.imageCache[value.value] && EpicApplication.imageCache[value.value].get(`${date.value}`)){
      addLI(EpicApplication.imageCache[value.value].get(`${date.value}`),menu);
      console.log("cache");
    } else{
      fetch(`https://epic.gsfc.nasa.gov/api/${value.value}/date/${date.value}`
      ).then((r)=>r.json()).then((data)=>{
        if(data.length === 0){
          menu.textContent = "no data found";
          return;
        }
        console.log("fetch");
        // const EpicApplication = {
        //   imageType: null,
        //   imageCache: {}
        // };
          addLI(data, menu);
          if (!EpicApplication.imageCache[value.value]){
            EpicApplication.imageCache[value.value] = new Map();
          }
          EpicApplication.imageType = value.value;
          EpicApplication.imageCache[EpicApplication.imageType].set(date.value, data);
  
      }).catch((r)=>{
        console.log(`my error is ${r}`);
      });
    }
  });
  menu.addEventListener("click",function(e){
    e.preventDefault();
    if(e.target.tagName !== "SPAN"){
      return;
    }
    let indexes = e.target.getAttribute("alex");
    let img = EpicApplication.imageCache[EpicApplication.imageType].get(`${date.value}`)[indexes].image;
    let dateArray = date.value.split("-");
    let year = dateArray[0];
    let month = dateArray[1];
    let day = dateArray[2];
    document.querySelector("#earth-image").src = `https://epic.gsfc.nasa.gov/archive/${EpicApplication.imageType}/${year}/${month}/${day}/jpg/${img}.jpg`;
    footDate.textContent = EpicApplication.imageCache[EpicApplication.imageType].get(`${date.value}`)[indexes].date;
    footTitle.textContent = EpicApplication.imageCache[EpicApplication.imageType].get(`${date.value}`)[indexes].caption;
  });
  value.addEventListener("change",() => { change(value, date);});
  function change(value, date) {
    menu.textContent = undefined;
    if(maxDate[`${value.value}`]){
      date.setAttribute("max",maxDate[`${value.value}`])
      date.value = maxDate[`${value.value}`];
      console.log("date cache");
    } else{
      fetch(`https://epic.gsfc.nasa.gov/api/${value.value}/all`
      ).then((r) => r.json()).then((data) => {
        data.sort((a, b) => { return b.date - a.date; });
        date.setAttribute("max", data[0].date);
        date.value = data[0].date;
        maxDate[`${value.value}`] = data[0].date;
        console.log("date fetch");
      }).catch((r) => {
        console.log(`My error is ${r}`);
      });
    }
  }
});
function addLI(data, menu) {
  for (let alex in data) {
    let li = document.querySelector("#image-menu-item").content.cloneNode(true);
    li.children[0].firstChild.textContent = data[alex].date;
    li.children[0].firstChild.setAttribute("alex", alex);
    menu.appendChild(li);
  }
}