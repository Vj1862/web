document.addEventListener("DOMContentLoaded", function() {
  const myStudent = document.getElementById("Student"); //Gave the p1 an id to easily find it
  const defaultValueMyStudent = document.getElementById("Student").textContent;
  const hover = document.querySelector("#hover");
  const mainText = document.getElementById("maintext");
  const button = document.getElementById("button");

  document.querySelector("footer").style.fontSize = "10pt";
  let size = document.querySelector("footer").style.fontSize;
  
  myStudent.addEventListener("mouseenter", function(){
    myStudent.textContent = "Vijay Patel";
  });
  myStudent.addEventListener("mouseleave",function(){
    myStudent.textContent = defaultValueMyStudent;
  })  
  function showAside(){
    hover.style.visibility = 'visible';
  }
  function hideAside(){
    hover.style.visibility = 'hidden';
  }
  mainText.addEventListener("mouseenter",showAside);
  mainText.addEventListener("mouseleave",hideAside);

  button.addEventListener("click",function(){
    size = Number(size.substring(0,size.length -2));
    size = (size + 4) + 'pt';
    document.querySelector("footer").style.fontSize = size;
  });


});