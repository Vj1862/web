document.addEventListener("DOMContentLoaded", function() {

  const unorderedList = document.querySelector("ul");
  const input = document.querySelector("#item");
  input.setAttribute("required", true); // OR input.required = true;
  const button = document.querySelector("#add-field");

  button.addEventListener("click",function(){
    let value = input.value;
    const list = document.createElement('li');
    const text = document.createTextNode(value);
    list.appendChild(text);
    unorderedList.appendChild(list);
    input.value = "";
  });

  const budget = document.querySelector("#div2");
  const buttonBudget = document.querySelector("#add-budget");

  buttonBudget.addEventListener("click",function(){
    const moner = document.querySelector("#money");
    let value = moner.value;
    const p = document.createElement('p');
    let text = document.createTextNode(value);
    text = "Budget: " + text + "$";
    p.appendChild(text);
    budget.appendChild(p);
    moner.value = "";
  });
});