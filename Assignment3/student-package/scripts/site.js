'use strict';
/**
 * description here
 * @author your name
 */
document.addEventListener("DOMContentLoaded", function(){
  let tbody = document.querySelector("#table-here");
  let table = document.createElement("table");

  const form = document.querySelector("form");

  form.addEventListener("submit",function(e){
    e.preventDefault();
    const row = e.target.Rows.value;
    const column = e.target.Columns.value;
    const first = e.target.even.value;
    const second = e.target.odd.value;

    table.remove();
    createTable(row,column,first,second)
  });

  function createTable(row,column,first,second){

    for(let i = 0;i<row;i++){
      let tr = document.createElement("tr");
      for(let j = 0;j<column;j++){
        let td = document.createElement("td");
        td.textContent = `${i},${j}`;
        
        if((i+j)%2 === 0){
          td.style.backgroundColor = first;
        }
        else{
          td.style.backgroundColor = second;
        }

        tr.appendChild(td);
      }
      table.appendChild(tr);
    }
    tbody.appendChild(table);
  }

});
